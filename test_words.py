#!/usr/bin/env python3

import unittest

from unittest.mock import patch, mock_open

from words import open_file_as_text, parse_text, get_mode, character_counter


class TestOpenFileAsText(unittest.TestCase):
    @patch('words.open', side_effect=Exception('Nope!'))
    def test_open_file_as_text_with_exception(self, open_mock):
        result = open_file_as_text('filename')

        open_mock.assert_called_with('filename')
        self.assertEqual('', result)

    @patch('words.open', new_callable=mock_open, read_data='THIS IS TEXT')
    def test_open_file_as_text_success(self, open_mock):
        result = open_file_as_text('filename')

        open_mock.assert_called_with('filename')
        self.assertEqual('THIS IS TEXT', result)


class TestParseText(unittest.TestCase):
    def test_parse_text_None_text(self):
        result = parse_text(None)

        self.assertEqual([], result)

    def test_parse_empty_text(self):
        result = parse_text('')

        self.assertEqual([], result)

    def test_parse_text_non_text(self):
        result = parse_text(100)

        self.assertEqual([], result)

    def test_parse_text_single_word(self):
        input = 'word'
        parsed = parse_text(input)

        self.assertEqual([('word', 'word')], parsed)

    def test_parse_text_simple(self):
        input = 'this is a little test.'
        parsed = parse_text(input)

        self.assertEqual(
            [
                ('this', 'this'),
                ('is', 'is'),
                ('a', 'a'),
                ('little', 'little'),
                ('test.', 'test')
            ],
            parsed
        )

    def test_parse_text_with_hyphenated_word(self):
        input = 'this is a little testy-test'
        parsed = parse_text(input)

        self.assertEqual(
            [
                ('this', 'this'),
                ('is', 'is'),
                ('a', 'a'),
                ('little', 'little'),
                ('testy-test', 'testy-test')
            ],
            parsed
        )

    def test_parse_text_with_an_apostrophe(self):
        input = "this is a lil' testy-test"
        parsed = parse_text(input)

        self.assertEqual(
            [
                ('this', 'this'),
                ('is', 'is'),
                ('a', 'a'),
                ("lil'", 'lil'),
                ('testy-test', 'testy-test')
            ],
            parsed
        )

    def test_parse_text_with_unicode_words(self):
        input = 'человек ここに　えいごおはなせるひとはいますか'
        parsed = parse_text(input)

        self.assertEqual([], parsed)

    def test_parse_text_with_numbers(self):
        input = 'there are 300 words here'
        parsed = parse_text(input)

        self.assertEqual(
            [
                ('there', 'there'),
                ('are', 'are'),
                ('words', 'words'),
                ('here', 'here')
            ],
            parsed
        )

    def test_parse_text_with_punctuation(self):
        input = "this is my test!!!"
        parsed = parse_text(input)

        self.assertEqual(
            [
                ('this', 'this'),
                ('is', 'is'),
                ('my', 'my'),
                ('test!!!', 'test')
            ],
            parsed
        )

    def test_parse_text_with_tabs(self):
        input = 'this is\tmy test'
        parsed = parse_text(input)

        self.assertEqual(
            [
                ('this', 'this'),
                ('is', 'is'),
                ('my', 'my'),
                ('test', 'test')
            ],
            parsed
        )

    def test_parse_text_with_newlines_and_tabs(self):
        input = 'this is my\n\n\n\n\ttest\t\tok.'
        parsed = parse_text(input)

        self.assertEqual(
            [
                ('this', 'this'),
                ('is', 'is'),
                ('my', 'my'),
                ('test', 'test'),
                ('ok.', 'ok')
            ],
            parsed
        )


class TestGetMode(unittest.TestCase):
    def test_get_mode_None(self):
        result = get_mode(None)

        self.assertEqual(None, result)

    def test_get_mode_empty_string(self):
        result = get_mode('')

        self.assertEqual(None, result)

    def test_get_mode_lowercase_words(self):
        result = get_mode('adam')

        self.assertEqual(2, result)

    def test_get_mode_duplicate_modes(self):
        result = get_mode('aaabbbcccddd')

        self.assertEqual(3, result)

    def test_get_mode_wherefore(self):
        result = get_mode('wherefore')

        self.assertEqual(3, result)


class TestCounter(unittest.TestCase):
    def test_romeo(self):
        result = character_counter('O Romeo, Romeo, wherefore art thou Romeo?')

        self.assertEqual('wherefore', result)

    def test_rain(self):
        result = \
            character_counter(
                'Some people feel the rain, while others just get wet.'
            )

        self.assertEqual('people', result)

    def test_blue_collar(self):
        result = character_counter('Blue-collar coal miners')

        self.assertEqual('Blue-collar', result)

    def test_single_word(self):
        result = character_counter('Word')

        self.assertEqual('Word', result)


if __name__ == '__main__':
    unittest.main()
