#!/usr/bin/env python3

import re
import sys


def character_counter(text):
    """
    This is the main function.  It orchestrates the flow through the functions.
    """
    winner = ('', 0)

    for word, parsed_word in parse_text(text):
        mode = get_mode(parsed_word)

        if mode > winner[1]:
            winner = (word, mode)

    return winner[0]


def open_file_as_text(filename):
    """
    Opens the provided filename and returns the text contents if the file
    exists.  Else, returns ''
    """
    try:
        with open(filename) as file:
            return file.read()
    except:
        return ''


def parse_text(text):
    """
    Splits the full text into words in two steps:
        1.  Matches on non-spaces to get full non-space words
            (we will want these full words for the end result)
        2.  Matches on only words to filter out numbers
            (we will want these cleaned words for the mode analysis)
    """
    if text is None or type(text) is not str:
        return []

    results = []
    spaces_regex = re.compile('\S+')
    clean_regex = re.compile('[A-Za-z-]+')

    for word in spaces_regex.findall(text):
        match = clean_regex.match(word)

        if match:
            results.append((word, match.group(0)))

    return results


def get_mode(word):
    """
    Gets the mode of the word and returns it

    NOTE:  This function gets the mode on the lowercase version of the word
    """
    if not word:
        return None

    lower_word = word.lower()
    modes = [lower_word.count(character) for character in set(lower_word)]

    return max(modes)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        result = character_counter(open_file_as_text(sys.argv[1]))

        print(result)
    else:
        print('Please pass in a filename to open')
